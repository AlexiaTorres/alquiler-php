<?php namespace App;
use App\Models\Rent;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RentCommand extends Command
{
    public function configure()
    {
        $this->setName('rent')
            ->setDescription('Alquilar un libro o peli')
            ->setHelp('Para alquilar un producto escribe el comando rent seguido del título y de tu nombre de usuario. Ejemplo: rent El_principito Alexia')
            ->addArgument('title', InputArgument::REQUIRED, 'Título del producto a alquilar')
            ->addArgument('name', InputArgument::REQUIRED, 'Nombre del usuario que alquila');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $rent = new Rent(
            $input->getArgument('title'),
            $input->getArgument('name')
        );
        $output->writeln($rent->save());
    }
}