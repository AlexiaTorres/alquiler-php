<?php
namespace App\Models;

class User {
    public $name;
    public $category;
    public $age;
    public $id;
    public static $file = 'data/users.json';

    public function __construct($name, $age, $category)
    {
        $this->name = $name;
        $this->category = $category;
        $this->age = $age;
        $this->id = time();
    }

    public function save()
    {
        $users = User::getUsers();

        $itemData = array(
            'name' => $this->name,
            'age' => $this->age,
            'category' => $this->category,
            'id' => $this->id,
        );

        array_push($users, $itemData);
        file_put_contents(User::$file, json_encode($users));
    }

    static function getUser($name)
    {
        $jsondata = file_get_contents(User::$file);
        $products = json_decode($jsondata, true);
        $filteredProducts = array_filter($products, function($product) use ($name) {
            return $product['name'] === $name;
        });
        return array_pop($filteredProducts);
    }

    static function getUsers()
    {
        $jsondata = file_get_contents(User::$file);
        $users = json_decode($jsondata, true);
        return $users;
    }
}