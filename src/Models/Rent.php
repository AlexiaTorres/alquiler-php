<?php
namespace App\Models;

class Rent {
    public $user;
    public $product;
    public static $file = 'data/rents.json';

    public function __construct($productTitle, $userName)
    {
        $this->product = Product::getPoduct($productTitle);
        $this->user = User::getUser($userName);
    }


    public function save()
    {
        $this->check();
        $rents = Rent::getRentedProducts();

        $itemData = array(
            'title' => $this->product['title'],
            'name' => $this->user['name']
        );

        array_push($rents, $itemData);
        file_put_contents(Rent::$file, json_encode($rents));

        return $this->getPrice();
    }

    private function check()
    {
        if(!$this->product) {
            throw new \Exception('No existe el producto');
        }

        if(!$this->user) {
            throw new \Exception('No existe el ususario');
        }
        $this->checkAge();

        $rentedProduct = Rent::getRentedProduct($this->product['title']);
        if($rentedProduct) {
            $message = 'No está disponible, ya está alquilado por otro usuario, te informaremos cuando esté libre';
            throw new \Exception($message);
        }

        $userHasRent = Rent::userHasRent($this->user['name']);
        if($userHasRent) {
            $message = 'Ya tienes alquilado un producto, para alquilar otra cosa debes devolverlo.';
            throw new \Exception($message);
        }
    }

    private function checkAge()
    {
        $age = $this->user['age'];
        $category = $this->product['category'];
        if (($category === 'niños') && (($age >= 0) && ($age <= 12))) {
            return true;
        }
        if (($category === 'jovenes') && (($age >= 13) && ($age <= 17))) {
            return true;
        }
        if (($category === 'adultos') && (($age >= 18))) {
            return true;
        }
        $message = 'No puedes alquilar este producto, pues su categoría es ' . $category . ' y tu edad no está en ese rango';
        throw new \Exception($message);
    }

    private function getPrice(){
        $userCategory = $this->user['category'];
        $productType = $this->product['type'];

        $disccountText = 'Como eres trabajador te aplicaremos un 10% de descuento al finalizar el pago.';
        $bookText = 'Procederemos al alquiler del libro, el precio es 3.5€. Gracias por confiar en nosotros después de tantos años :")';
        $filmText = 'Procederemos al alquiler de la peli, el precio es 5€. Gracias por confiar en nosotros después de tantos años :")';

        if ($userCategory === 'usuario') {
            if ($productType === 'libro') {
                $message = $bookText;
            } else {
                $message = $filmText;
            }
        } else {
            if ($productType === 'libro') {
                $message = $bookText . ' ' . $disccountText;
            } else {
                $message = $filmText . ' ' . $disccountText;
            }
        }
        return $message;
    }

    static function getRentedProduct($productName) {
        $jsondata = file_get_contents(Rent::$file);
        $rents = json_decode($jsondata, true);
        $filteredRents = array_filter($rents, function($rent) use ($productName) {
            return $rent['title'] === $productName;
        });
        return array_pop($filteredRents);
    }

    static function userHasRent($userName) {
        $jsondata = file_get_contents(Rent::$file);
        $rents = json_decode($jsondata, true);
        $filteredRents = array_filter($rents, function($rent) use ($userName) {
            return $rent['name'] === $userName;
        });
        return array_pop($filteredRents);
    }

    static function getRentedProducts()
    {
        $jsondata = file_get_contents(Rent::$file);
        $rents = json_decode($jsondata, true);
        return $rents;
    }
}