<?php
namespace App\Models;

class ReturnProduct {
    public $user;
    public $product;
    public static $file = 'data/rents.json';

    public function __construct($productTitle, $userName)
    {
        $this->product = Product::getPoduct($productTitle);
        $this->user = User::getUser($userName);
    }

    public function save()
    {
        $this->check();
        $rents = ReturnProduct::getRentedProducts();
        $name = $this->user['name'];

        $updatedRents = array_filter($rents, function ($k) use ($name) {
            return $k['name'] !== $name;
        });

        $rents = [];
        foreach ($updatedRents as $item) {
            array_push($rents, $item);
        }
        file_put_contents(ReturnProduct::$file, json_encode($rents));

    }

    private function check()
    {
        if(!$this->product) {
            throw new \Exception('No existe el producto');
        }

        if(!$this->user) {
            throw new \Exception('No existe el ususario');
        }

        $userHasRent = ReturnProduct::userHasRent($this->user['name']);

        if(!$userHasRent || $userHasRent === null) {
            $message = 'No tienes nada alquilado.';
            throw new \Exception($message);
        }
    }

    static function userHasRent($userName) {
        $jsondata = file_get_contents(ReturnProduct::$file);
        $rents = json_decode($jsondata, true);
        $filteredRents = array_filter($rents, function($rent) use ($userName) {
            return $rent['name'] === $userName;
        });
       return $filteredRents;
    }

    static function getRentedProducts()
    {
        $jsondata = file_get_contents(ReturnProduct::$file);
        $rents = json_decode($jsondata, true);
        return $rents;
    }
}