<?php
namespace App\Models;

class Product {
    public $title;
    public $category;
    public $type;
    public $id;
    public static $file = 'data/products.json';

    public function __construct($title, $category, $type)
    {
        $this->category = $category;
        $this->title = $title;
        $this->type = $type;
        $this->id = time();
    }

    public function save()
    {
        $products = Product::getProducts();

        $itemData = array(
            'id' => $this->id,
            'title' => $this->title,
            'type' => $this->type,
            'category' => $this->category,
        );

        array_push($products, $itemData);
        file_put_contents(Product::$file, json_encode($products));
    }

    static function getPoduct($title)
    {
        $jsondata = file_get_contents(Product::$file);
        $products = json_decode($jsondata, true);
        $filteredProducts = array_filter($products, function($product) use ($title) {
            return $product['title'] === $title;
        });
        return array_pop($filteredProducts);
    }

    static function getProducts()
    {
        $jsondata = file_get_contents(Product::$file);
        $products = json_decode($jsondata, true);
        return $products;
    }
}