<?php namespace App;
use App\Models\Product;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProductCommand extends Command
{
    public function configure()
    {
        $this -> setName('product')
            -> setDescription('Añadir nuevo libro o peli')
            -> setHelp('Si quieres añadir un libro a la biblioteca usa el comando "product" seguido del título, tipo y categoría, por ejemplo: product El_principito libro jovenes')
            -> addArgument('title', InputArgument::REQUIRED, 'Título del libro o peli. Usando _ en lugar de espacios')
            -> addArgument('type', InputArgument::REQUIRED, 'Puede ser libro o peli')
            -> addArgument('category', InputArgument::REQUIRED, 'Categoría. Puede ser niños, jovenes o adultos');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $product = new Product(
            $input->getArgument('title'),
            $input->getArgument('category'),
            $input->getArgument('type')
        );
        $product->save();
        $output->write('Añadido a la biblioteca!');
    }
}
