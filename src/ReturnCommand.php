<?php namespace App;

use App\Models\ReturnProduct;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReturnCommand extends Command
{
    public function configure()
    {
        $this->setName('return')
            ->setDescription('Devolver un libro o peli')
            ->setHelp('Para devolver un producto escribe el comando return seguido del título y de tu nombre de usuario. Ejemplo: return Principito Alexia')
            ->addArgument('title', InputArgument::REQUIRED, 'Título del producto a devolver')
            ->addArgument('name', InputArgument::REQUIRED, 'Usuario que devuelve alquiler');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $returnProduct = new ReturnProduct(
            $input->getArgument('title'),
            $input->getArgument('name')
        );

        $returnProduct->save();
        $output->writeln('Producto devuelto :)');
    }
}
