<?php namespace App;
use App\Models\User;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UserCommand extends Command
{
    public function configure()
    {
        $this -> setName('user')
            -> setDescription('Añadir un nuevo usuario.')
            -> setHelp('Aquí añadimos al usuario, escribiendo el comando user seguido del nombre, edad y categoría(usuario o trabajador), quedando por ejemplo: user Alexia 27 usuario')
            -> addArgument('name', InputArgument::REQUIRED, 'Nombre del usuario')
            -> addArgument('age', InputArgument::REQUIRED, 'Edad')
            -> addArgument('category', InputArgument::REQUIRED, 'Categoría. Puede ser usuario o trabajador');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $user = new User(
            $input->getArgument('name'),
            $input->getArgument('category'),
            $input->getArgument('age')
        );
        $user->save();

        $output->writeln('Usuario añadido');
    }
}