#!/usr/bin/env php
<?php
require_once __DIR__ . '/vendor/autoload.php';
use Symfony\Component\Console\Application;
use App\ProductCommand;
use App\UserCommand;
use App\RentCommand;
use App\ReturnCommand;

$app = new Application('*****Alquiler libros y pelis****', 'v1.0.0');
$app -> add(new ProductCommand());
$app -> add(new UserCommand());
$app -> add(new RentCommand());
$app -> add(new ReturnCommand());
$app -> run();