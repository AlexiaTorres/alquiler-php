# Alquiler libros y pelis

Esta es una aplicación de consola, todo funciona con comandos :)

## Clonar el repositorio o descargar el zip  

  `git clone https://gitlab.com/AlexiaTorres/alquiler-php.git`  


## Dentro de alquiler-php/ ya puedes empezar a ejecutar comandos.  

  Todo comando se ejecuta con ` php ./console.php ` delante.  

  Ejemplo:  

  `php ./console.php list`  


## Comandos disponibles:    

- Listado de comandos:  
` php ./console.php list `  

- Descripción y cómo usar cada comando:  
    `php ./console.php <nombrecomando> --help`  

- Crear un nuevo usuario:  

` php ./console.php user <name> <age> <category>`  

donde `<name>` es el nombre del usuario, `<age>` la edad y `<category>` puede ser trabajador o usuario.  


- Crear un nuevo libro o peli:  

` php ./console.php product <title> <type> <category>`  

donde `<title>` es el titulo del producto, `<type>` puede ser libro o peli, `<category>` puede ser niños, jovenes o adultos.  


- Alquilar un libro o peli:  

` php ./console.php rent <title> <name>`  

donde `<title>` es el título del producto y `<name>` el nombre del usuario.  


- Devolver un libro o peli:  

` php ./console.php return <title> <name>`  

donde `<title>` es el título del producto y `<name>` es el nombre del usuario.





